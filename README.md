# README #

### Installation ###


```
#!bash

git clone https://przemekkraw@bitbucket.org/przemekkraw/travelfinder.git
pip install travelfinder/
```
Other requirements: PhantomJS browser.


### Usage ###

```
#!python

from travelfinder import search_trip

res = search_trip(departure_city, arrival_city, min_days, max_days, max_price, start_date, end_date)
print(res)
res.toCSV('results.csv')
res.toJSON('results.json')
```

Example:


```
#!python

res = search_trip('Budapest', 'Baku', 4, 14, 500, end_date='2016-10-01')
res = search_trip('Katowice', 'Kutaisi', 4, 10, 400, start_date='2016-08-21', end_date='2016-10-01')
```