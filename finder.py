import random
from datetime import date
from .wizzair import crazy_run, print_flights


cities = ['Warsaw', 'Gdansk', 'Katowice']


def random_date():
    while True:
        day = random.randint(1,31)
        month = random.randint(5,12)
        year = 2016
        try:
            d = str(date(year, month, day))
            today = str(date().today())
            if d > today
                return d
        except ValueError:
            pass


def random_city():
    return cities[random.randint(0, len(cities)-1)]


def search(max_price, max_len, max_day):
    print_flights(crazy_run(
                random_city(),
                random_date(),
                max_price,
                max_len,
                max_day
                ))


def run():
    while True:
        search(120.00, 5, 4)
        time.sleep(10)
