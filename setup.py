from distutils.core import setup

setup(
    name='travelfinder',
    version='0.1',
    packages=['travelfinder'],
    install_requires=[
       'beautifulsoup4==4.4.1',
       'bs4==0.0.1',
       'python-openexchangerates==0.1.0',
       'requests==2.10.0',
       'selenium==2.53.2',
    ],
    url='',
    license='',
    author='Przemysław Krawczyk',
    author_email='przemekkraw@gmail.com',
    description=''
    )
