from datetime import datetime
from openexchangerates.exchange import Exchange

from travelfinder.wizzair.globals import currency_names
from bs4 import BeautifulSoup as bs


class WizzParser:

    def __init__(self, currency='PLN'):
        self.exchanger = Exchange()
        self.currency = currency

    def __find_date(self, cell):
        return cell.find('strong').text

    def parse_price(self, price):
        value, currency = price.rsplit(' ', 1)
        value = float(value.replace(',', '.').replace('\xa0', ''))
        return round(
                self.exchanger.exchange(
                    value,
                    currency_names[currency],
                    self.currency),
                2
                )

    def parse_timetable_cell(self, cell):
        res = dict()
        res['date'] = self.__find_date(cell)
        item = cell.find('span', {'class': 'item'})
        price = cell.find('span', {'class': 'price'})
        if item and price:
            res['departure_city'] = item.attrs['data-departurestation']
            res['arrival_city'] = item.attrs['data-arrivalstation']
            d,a = item.attrs['data-time'].split('|')
            res['departure_time'], res['arrival_time'] = (d,a)
            res['price'] = self.parse_price(price.text)
        return res

    def parse_timetable(self, timetable):
        res = list()
        for cell in timetable:
            flight = self.parse_timetable_cell(cell)

            res.append(flight)
        return res

    def get_last_date(self, timetable):
        return self.__find_date(timetable[-1])

    def get_timetable(self, html):
        page = bs(html, 'html.parser')
        return page.findAll('span', {'class': 'flights_daylist'})

    def filter_flights(self, flights_list, max_price=None, start_date=None,
                       end_date=None):
        res = list()
        if not start_date:
            start_date = datetime.now().strftime('%Y-%m-%d')
        if end_date and start_date > end_date:
            raise ValueError("end_date must be greater than start_date")
        for flight in flights_list:
            if flight['date'] < start_date:
                continue
            if end_date and flight['date'] > end_date:
                break
            if not 'price' in flight.keys():
                continue
            if max_price and flight['price'] > max_price:
                continue
            res.append(flight)
        return res

