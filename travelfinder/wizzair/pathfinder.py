import time
import random
import datetime
from .globals import cities, directions, airports, currency_names
from .wizzair import Wizzair


def get_directions_from_city(city, countries=None):
    res = list()
    if city not in directions.keys():
        return res
    if not countries:
        countries = cities.keys()
    for country, dirs in directions[city].items():
        if country in countries:
            res += dirs
    return res


def get_directions_to_city(city, countries=None):
    res = list()
    if city not in directions.keys():
        return res
    if not countries:
        countries = cities.keys()
    for c, d in directions.items():
        for k, v in d.items():
            if city in v:
                res.append(c)
    return res


def __add_date(date, days):
    y,m,d = (int(el) for el in date.split('-'))
    current_date = datetime.date(y,m,d)
    new_date = current_date + datetime.timedelta(days=days)
    return str(new_date)


def __min_price(flights, max_price=400.00):
    min_flight = None
    min_price = max_price
    for idx, flight in enumerate(flights):
        #print(flight)
        if flight[4] < min_price:
            min_price = flight[4]
            min_flight = flight
    return (min_price, min_flight)


def __crazy_run(start_city, start_country, start_date, wizz, res, max_price, min_len=2, max_len=5, day=3):
    #print(res)
    if max_len > 0:
        all_dirs = get_directions_from_city(start_city)
        return_dirs = get_directions_from_city(start_city, start_country)
        dirs = return_dirs if max_len == 1 and return_dirs else [el for el in all_dirs if el not in return_dirs]
        random.shuffle(dirs)

        for dir_city in dirs:
            if dir_city in res['path']:
                continue
            print('{0} -> {1}, start: {2}, end: {3}'.format(start_city, dir_city, start_date, __add_date(start_date,3)))
            flights = wizz.search_by_city(start_city, dir_city, __add_date(start_date, day), start_date)
            time.sleep(2)
            if flights:
                price, flight = __min_price(flights)
                print('min_price: {0}, max_price {1}'.format(price, max_price))
                if price < max_price:
                    res['path'].append(dir_city)
                    res['flights'].append(flight)
                    __crazy_run(dir_city, start_country, flight[3], wizz, res, max_price, min_len, max_len-1)
                    break

def __get_country(city):
    for country, dirs in cities.items():
        if city in dirs:
            return country


def crazy_run(start_city, start_date, max_price=100.00, min_len=2, max_len=5, day=3):
    w = Wizzair()
    res = dict()
    res['path'] = list()
    res['flights'] = list()
    start_country = __get_country(start_city)
    __crazy_run(start_city, start_country, start_date, w, res, max_price, min_len, max_len, day)
    return res['flights']


def print_flights(flights):
    for flight in flights:
        print('{0} -> {1}, {2}, {3}, {4}'.format(
            flight[0],
            flight[1],
            flight[3],
            flight[2],
            flight[4]
            ))
