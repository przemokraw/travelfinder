cities = {
        'Belgium': ['Brussels'],
        'Bulgaria': ['Sofia'],
        'Croatia': ['Split'],
        'Cyprus': ['Larnaca'],
        'Czechia': ['Prague'],
        'Danmark': ['Billund'],
        'England': ['London', 'Bristol', 'Birmingham'],
        'Finland': ['Turku'],
        'France': ['Paris', 'Nice', 'Grenoble'],
        'Georgia': ['Kutaisi'],
        'Germany': ['Dortmund', 'Frankfurt', 'Berlin'],
        'Hungary': ['Budapest'],
        'Iceland': ['Reykjavik'],
        'Italy': ['Alghero', 'Bologna', 'Bari', 'Catania', 'Rome', 'Milan', 'Naples'],
        'Netherland': ['Eindhoven', 'Maastricht', 'Groningen'],
        'Norway': ['Alesund', 'Bergen', 'Molde', 'Stavanger', 'Oslo'],
        'Poland': ['Gdansk', 'Katowice', 'Poznan', 'Warsaw', 'Lublin', 'Wroclaw'],
        'Portugal': ['Lisbon', 'Porto'],
        'Slovakia': ['Kosice', 'Poprad'],
        'Slovenia': ['Ljubljana'],
        'Spain': ['Lanzarote', 'Malaga', 'Alicante', 'Barcelona', 'Fuertaventura', 'Madrit', 'Tenerife'],
        'Sweden': ['Gothenburg', 'Malmo', 'Stockholm'],
        'Switzerland': ['Basel', 'Geneva'],
        }


directions = {
        'Alesund': {
            'Poland': ['Gdansk'],
            },
        'Alicante': {
            'Hungary': ['Budapest'],
            'Poland': ['Warsaw'],
            },
        'Alghero': {
            'Hungary': ['Budapest'],
            'Poland': ['Katowice', 'Warsaw'],
            },
        'Barcelona': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            'Poland': ['Katowice', 'Warsaw', 'Gdansk', 'Poznan'],
            },
        'Bari': {
            'Bulgaria': ['Sofia'],
            'Czechia': ['Prague'],
            'Hungary': ['Budapest'],
            'Poland': ['Warsaw'],
            },
        'Basel': {
            'Bulgaria': ['Sofia'],
            'Poland': ['Warsaw'],
            },
        'Bergen': {
            'Poland': ['Gdansk', 'Katowice', 'Warsaw'],
            },
        'Berlin': {
            'Georgia': ['Kutaisi'],
            },
        'Billund': {
            'Bulgaria': ['Sofia'],
            'Poland': ['Gdansk'],
            },
        'Birmingham': {
            'Poland': ['Warsaw', 'Poznan'],
            },
        'Bristol': {
            'Poland': ['Warsaw', 'Katowice'],
            },
        'Bologna': {
            'Hungary': ['Budapest'],
            'Poland': ['Katowice'],
            },
        'Brussels': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Warsaw'],
            'Slovenia': ['Ljubljana'],
            },
        'Budapest': {
            'Belgium': ['Brussels'],
            'Bulgaria': ['Sofia'],
            'Cyprus': ['Larnaka'],
            'England': ['Birmingham', 'London'],
            'France': ['Nice', 'Paris'],
            'Germany': ['Dortmund', 'Frankfurt'],
            'Georgia': ['Kutaisi'],
            'Iceland': ['Reykjavik'],
            'Italy': ['Alghero', 'Bologna', 'Bari', 'Catania', 'Rome', 'Milan', 'Naples'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Poland': ['Warsaw'],
            'Portugal': ['Lisbon'],
            'Spain': ['Lanzarote', 'Malaga', 'Alicante', 'Barcelona', 'Fuertaventura', 'Madrit', 'Tenerife'],
            'Sweden': ['Gothenburg', 'Malmo', 'Stockholm'],
            },
        'Catania': {
            'Hungary': ['Budapest'],
            'Poland': ['Warsaw'],
            },
        'Dortmund': {
            'Bulgaria': ['Sofia'],
            'Georgia': ['Kutaisi'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Katowice', 'Warsaw', 'Wroclaw'],
            },
        'Eindhoven': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Katowice', 'Poznan', 'Warsaw', 'Wroclaw'],
            },
        'Frankfurt': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Katowice'],
            },
        'Fuertaventura': {
            'Hungary': ['Budapest'],
            },
        'Gdansk': {
            'Belgium': ['Brussels'],
            'Danmark': ['Billund'],
            'England': ['London'],
            'Finland': ['Turku'],
            'France': ['Paris'],
            'Germany': ['Dortmund', 'Frankfurt'],
            'Iceland': ['Reykjavik'],
            'Italy': ['Milan', 'Rome'],
            'Netherland': ['Eindhoven', 'Groningen', 'Maastricht'],
            'Norway': ['Alesund', 'Bergen', 'Molde', 'Stavanger', 'Oslo'],
            'Spain': ['Barcelona'],
            'Sweden': ['Gothenberg', 'Malmo', 'Stockholm'],
            },
        'Geneva': {
            'Poland': ['Warsaw'],
            },
        'Gothenburg': {
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Warsaw'],
            },
        'Grenoble': {
            'Poland': ['Warsaw'],
            },
        'Groningen': {
            'Poland': ['Gdansk'],
            },
        'Katowice': {
            'Cyprus': ['Larnaka'],
            'England': ['London', 'Birmingham', 'Bristol'],
            'France': ['Paris'],
            'Georgia': ['Kutaisi'],
            'Germany': ['Dortmund', 'Frankfurt'],
            'Italy': ['Milan', 'Rome', 'Alghero', 'Naples', 'Bologna'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Bergen', 'Stavanger', 'Oslo'],
            'Spain': ['Barcelona', 'Lanzarote', 'Tenerife'],
            'Sweden': ['Malmo', 'Stockholm'],
            },
        'Kosice': {
            'England': ['London'],
            },
        'Kutaisi': {
            'Cyprus': ['Larnaca'],
            'Germany': ['Dortmund', 'Berlin'],
            'Hungary': ['Budapest'],
            'Italy': ['Milan'],
            'Poland': ['Katowice', 'Warsaw'],
            },
        'Lanzarote': {
            'Hungary': ['Budapest'],
            'Poland': ['Katowice'],
            },
        'Larnaca': {
            'Bulgaria': ['Sofia'],
            'Georgia': ['Kutaisi'],
            'Hungary': ['Budapest'],
            'Poland': ['Katowice', 'Warsaw'],
            },
        'Lisbon': {
            'Poland': ['Warsaw'],
            },
        'Ljubljana': {
            'Belgium': ['Brussels'],
            },
        'London': {
            'Bulgaria': ['Sofia'],
            'Croatia': ['Split'],
            'Czechia': ['Prague'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Warsaw', 'Katowice', 'Lublin', 'Poznan', 'Wroclaw'],
            'Slovakia': ['Kosice', 'Poprad'],
            'Slovenia': ['Ljubljana'],
            },
        'Lublin': {
            'England': ['London'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Oslo'],
            'Sweden': ['Stockholm'],
            },
        'Maastricht': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Katowice', 'Poznan', 'Warsaw', 'Wroclaw'],
            },
        'Madrit': {
            'Bulgaria': ['Sofia'],
            'Hungary': ['Budapest'],
            },
        'Malaga': {
            'Hungary': ['Budapest'],
            },
        'Malmo': {
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Warsaw', 'Katowice', 'Poznan'],
            },
        'Milan': {
            'Bulgaria': ['Sofia'],
            'Czechia': ['Prague'],
            'Georgia': ['Kutaisi'],
            'Poland': ['Katowice', 'Warsaw', 'Gdansk', 'Poznan'],
            },
        'Molde': {
            'Poland': ['Gdansk'],
            },
        'Naples': {
            'Czechia': ['Prague'],
            'Hungary': ['Budapest'],
            'Poland': ['Katowice', 'Warsaw'],
            },
        'Nice': {
            'Hungary': ['Budapest'],
            },
        'Oslo': {
            'Poland': ['Gdansk', 'Katowice', 'Warsaw', 'Lublin', 'Poznan', 'Wroclaw'],
            'Bulgaria': ['Sofia'],
            },
        'Paris': {
            'Bulgaria': ['Sofia'],
            'Poland': ['Gdansk', 'Katowice', 'Poznan', 'Warsaw'],
            },
        'Poprad': {
            'England': ['London'],
            },
        'Porto': {
            'Hungary': ['Budapest'],
            'Poland': ['Warsaw'],
            },
        'Poznan': {
            'England': ['London', 'Birmingham'],
            'France': ['Paris'],
            'Italy': ['Milan'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Oslo'],
            'Spain': ['Barcelona'],
            'Sweden': ['Malmo', 'Stockholm'],
            },
        'Prague': {
            'England': ['London'],
            'Italy': ['Milan', 'Bari', 'Rome', 'Naples'],
            },
        'Reykjavik': {
            'Hungary': ['Budapest'],
            'Poland': ['Warsaw', 'Gdansk'],
            },
        'Rome': {
            'Bulgaria': ['Sofia'],
            'Czechia': ['Prague'],
            'Hungary': ['Budapest'],
            'Poland': ['Katowice', 'Warsaw', 'Gdansk'],
            },
        'Sofia': {
            'Belgium': ['Brussels'],
            'Cyprus': ['Larnaka'],
            'England': ['Birmingham', 'Bristol', 'London'],
            'France': ['Paris'],
            'Germany': ['Dortmund', 'Frankfurt'],
            'Georgia': ['Kutaisi'],
            'Hungary': ['Budapest'],
            'Italy': ['Milan', 'Bologna', 'Bari', 'Rome', 'Naples'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Oslo'],
            'Spain': ['Alicante', 'Barcelona', 'Madrit'],
            'Sweden': ['Malmo'],
            'Switzerland': ['Basel', 'Geneva'],
            },
        'Split': {
            'England': ['London'],
            },
        'Stavanger': {
            'Poland': ['Gdansk', 'Katowice'],
            },
        'Stockholm': {
            'Hungary': ['Budapest'],
            'Poland': ['Gdansk', 'Warsaw', 'Katowice', 'Poznan', 'Wroclaw', 'Lublin'],
            },
        'Tenerife': {
            'Hungary': ['Budapest'],
            'Poland': ['Katowice'],
            },
        'Turku': {
            'Poland': ['Gdansk'],
            },
        'Warsaw': {
            'Belgium': ['Brussels'],
            'Cyprus': ['Larnaca'],
            'England': ['London', 'Birmingham', 'Bristol'],
            'France': ['Paris', 'Grenoble'],
            'Georgia': ['Kutaisi'],
            'Germany': ['Dortmund'],
            'Hungary': ['Budapest'],
            'Iceland': ['Reykjavik'],
            'Italy': ['Milan', 'Rome', 'Alghero', 'Bari', 'Catania', 'Naples', 'Turin', 'Bologna'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Bergen', 'Oslo'],
            'Portugal': ['Lisbon', 'Porto'],
            'Spain': ['Barcelona', 'Alicante'],
            'Sweden': ['Gothenburg', 'Malmo', 'Stockholm'],
            'Switzerland': ['Basel', 'Geneva'],
            },
        'Wroclaw': {
            'England': ['London', 'Birmingham'],
            'Germany': ['Dortmund'],
            'Netherland': ['Eindhoven', 'Maastricht'],
            'Norway': ['Oslo'],
            'Sweden': ['Stockholm'],
            }
        }









# jeśli lotniskoma inną nazwę niż miasto lub jest więcej niż jendo w mieście
airports = {
        'Warsaw': ['Warsaw-Chopin'],
        'Rome': ['Rome-Fiumicino'],
        'Brussels': ['Brussels-Charleroi'],
        'London': ['London-Luton'],
        'Oslo': ['Oslo - Sandefjord - Torp'],
        'Rome': ['Rome-Fiumicino'],
        'Paris': ['Paris-Beauvais'],
        'Grenoble': ['Grenoble - Lyon'],
        'Frankfurt': ['Frankfurt - Hahn'],
        'Maastricht': ['Maastricht Aachen'],
        'Gothenburg': ['Gothenburg - Landvetter'],
        'Stockholm': ['Stockholm-Skavsta'],
        'Basel': ['Basel-Mulhouse-Freiburg'],
        'Frankfurt': ['Frankfurt - Hahn'],

        }


currency_names = {
        '€': 'EUR',
        'ft': 'HUF',
        'zł': 'PLN',
        '£': 'GBP',
        'Ft': 'HUF',
        'kr': 'NOK',
        'lv': 'BGN',
        }
