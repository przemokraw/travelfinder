import time
import datetime
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup as bs
from openexchangerates.exchange import Exchange

from travelfinder.wizzair.globals import airports
from travelfinder.wizzair.parser import WizzParser


class Wizzair:

    def __init__(self, currency='PLN'):
        self.drv = webdriver.PhantomJS()
        self._URL = 'https://wizzair.com/pl-PL/TimeTable/{0}/{1}'
        self.name = 'Wizzair'
        self.parser = WizzParser(currency)

    def __del__(self):
        self.drv.quit()

    def search_by_airport(self, dep_air, arr_air, max_price=None,
                          start_date=None, end_date = None, delay=5,
                          max_attempts=5):
        self.drv.get(self._URL.format(dep_air, arr_air))
        attempts = 0
        done = False
        while not done and attempts < max_attempts:
            try:
                WebDriverWait(self.drv, delay).until(
                        EC.presence_of_element_located(
                        (By.CLASS_NAME, 'next')))
                done = True
            except TimeoutException:
                attempts += 1

        timetable = self.parser.get_timetable(self.drv.page_source)
        if end_date:
            while self.parser.get_last_date(timetable) < end_date:
                self.drv.find_element_by_class_name('next').click()
                time.sleep(delay)
                timetable = self.parser.get_timetable(self.drv.page_source)
        
        flights = self.parser.parse_timetable(timetable)
        return self.parser.filter_flights(flights, max_price, start_date,
                                          end_date)

    def search_by_city(self, dep_city, arr_city, start_date=None,
                       end_date=None, max_price=None, delay=5, max_attempts=5):
        res = []
        dep_aiports = airports.get(dep_city, [dep_city])
        arr_aiports = airports.get(arr_city, [arr_city])
        for dep_airport in dep_aiports:
            for arr_airport in arr_aiports:
                res += self.search_by_airport(dep_airport, arr_airport,
                                              max_price, start_date, end_date,
                                              delay, max_attempts)
        return res
