import csv

class FlightsList:
    
    def __init__(self, flightslist=None, two_sided=False):
        self.two_sided= two_sided
        self.header = ['date', 'departure_time', 'arrival_time',
                       'departure_city', 'arrival_city', 'price']
        if not flightslist:
            self.flightslist = list()
        else:
            for item in flightslist:
                if set(item.keys()) != set(self.header):
                    raise ValueError("Keys do not match.")
            self.flightslist = flightslist if flightslist else list()

    def add_flight(self, flight):
        if set(flight.keys()) != set(self.header):
            raise ValueError("Keys do not match.")
        self.flightslist.append(flight)

    def add_fligts(self, flights):
        for item in flights:
            self.add_flight(item)

    def toCSV(self, filename):
        with open(filename, 'w') as f:
            dw = csv.DictWriter(f, self.header)
            dw.writeheader()
            dw.writerows(self.flightslist)

    def toJSON(self, filename):
        with open(filename, 'w') as f:
            json.dump(self.flightslist, f, indent=4)

    def __str__(self):
        res = ''
        if not self.two_sided:
            for flight in self.flightslist:
                res += '{0} -> {1}, {2}, {3} -> {4}, {5}\n'.format(
                            flight['departure_city'],
                            flight['arrival_city'],
                            flight['date'],
                            flight['departure_time'],
                            flight['arrival_time'],
                            flight['price']
                            )
        else:
            for i in range(0, len(self.flightslist), 2):
                res += '{0} - {1}; SUMA: {2} PLN; start: {3} {4}, {5} PLN; return: {6} {7}, {8} PLN;\n'.format(
                    self.flightslist[i]['departure_city'],
                    self.flightslist[i]['arrival_city'],
                    round(self.flightslist[i]['price'] + self.flightslist[i+1]['price'], 2),
                    self.flightslist[i]['date'],
                    self.flightslist[i]['departure_time'],
                    #self.flightslist[i]['arrival_time'],
                    self.flightslist[i]['price'],
                    self.flightslist[i+1]['date'],
                    self.flightslist[i+1]['departure_time'],
                    #self.flightslist[i+1]['arrival_time'],
                    self.flightslist[i+1]['price'])

        return res
