from datetime import date, timedelta
from travelfinder.wizzair import Wizzair
from travelfinder.flightslist import FlightsList


def __less(date1, date2, days):
    d1 = date(*[int(el) for el in date1.split('-')])
    d2 = date(*[int(el) for el in date2.split('-')])
    if d2 > d1 and d2 - d1 <= timedelta(days=days):
        return True
    else:
        return False


def __greater(date1, date2, days):
    d1 = date(*[int(el) for el in date1.split('-')])
    d2 = date(*[int(el) for el in date2.split('-')])
    if d2 > d1 and d2 - d1 >= timedelta(days=days):
        return True
    else:
        return False


def search_flights(dep_city, arr_city, max_price=None, start_date=None,
                   end_date=None, delay=5, max_attempts=5, output_csv=None,
                   output_json=None, return_flight=False):
    w = Wizzair()
    res = w.search_by_city(dep_city, arr_city, start_date, end_date,
                           max_price, delay, max_attempts)
    if return_flight:
        res += w.search_by_city(arr_city, dep_city, start_date, end_date,
                                max_price, delay, max_attempts)
    fl = FlightsList(res)
    if output_csv:
        fl.toCSV(output_csv)
    if output_json:
        fl.toJSON(output_json)
    if not output_csv and not output_json:
        return fl


def search_trip(dep_city, arr_city, min_days, max_days, max_price=200,
                start_date=None, end_date=None, output_csv=None,
                output_json=None):
    w = Wizzair()
    dep = w.search_by_city(dep_city, arr_city, start_date, end_date,
                           max_price, 5, 5)
    arr = w.search_by_city(arr_city, dep_city, start_date, end_date,
                           max_price, 5, 5)
    res = FlightsList(two_sided=True)
    for d in dep:
        if d['price'] < max_price:
            for a in arr:
                if __less(d['date'], a['date'], max_days):
                    if __greater(d['date'], a['date'], min_days):
                        if d['price'] + a['price'] < max_price:
                            res.add_flight(d)
                            res.add_flight(a)
    if output_csv:
        res.toCSV(output_csv)
    if output_json:
        res.toJSON(output_json)
    if not output_csv and not output_json:
        return res

